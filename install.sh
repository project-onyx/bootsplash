#!/usr/bin/env bash

COUNTER=1
EXT=png
PACKAGE="$(dirname "$0")"
SRC="$PACKAGE/onyx"
INSTALL_PATH="/usr/share/plymouth/themes"

for IMG in "$SRC"/*.$EXT ; do
	# printf -v NUM "%02d" $COUNTER
	if [ ! -f "$SRC"/progress-$COUNTER.$EXT ]; then
		mv "$IMG" "$SRC"/progress-$COUNTER.$EXT
	fi
	COUNTER=$(expr $COUNTER + 1)
done

COUNTER=1

for IMG in "$SRC"/throbber/*.$EXT ; do
	printf -v NUM "%04d" $COUNTER
	if [ ! -f "$SRC"/throbber/throbber-$NUM.$EXT ]; then
		mv "$IMG" "$SRC"/throbber/throbber-$NUM.$EXT
	fi
	COUNTER=$(expr $COUNTER + 1)
done

# if [ -f "$PACKAGE"/../mnt/fusauto.conf ]; then
# 	"$PACKAGE"/../mount.sh
# fi

if [ -d  "$PACKAGE/../mnt/usr" ]; then
	PREFIX="$PACKAGE/../mnt"
else
	if [ ! -d "$PACKAGE/build" ]; then
		mkdir "$PACKAGE/build"
	PREFIX="$PACKAGE/build"
fi

cp "$SRC" "$PREFIX/$INSTALL_PATH"

	
